Reveal.initialize({
	history: true,
	transition: 'linear',

	mathjax3: {
		mathjax: 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg-full.js',
		tex: {
			inlineMath: [['$', '$'], ['\\(', '\\)']], 
			macros: {
				R: '\\mathbb{R}',
				set: [ '\\left\\{#1 \\; ; \\; #2\\right\\}', 2 ]
			}
		},
		svg: {
			fontCache: 'local', 

		}, 
		options: {
		  skipHtmlTags: [ 'script', 'noscript', 'style', 'textarea', 'pre' ]
		},
	},

	// There are three typesetters available
	// RevealMath.MathJax2 (default)
	// RevealMath.MathJax3
	// RevealMath.KaTeX
	//
	// More info at https://revealjs.com/math/
	plugins: [ RevealMath.MathJax3 ]
});

let bg1 = document.getElementById("bg1-nodes");
let bg1Overlay = document.getElementById("bg1-overlay");
let init_pos = [[29, 127], [75, 87], [86, 131], [9, 170], [136, 35], [185, 46], [186, 0], [30, 30]];
let children = bg1.children;
let evLoop = step;

graphNodeInit(bg1, init_pos);

let lineIndexes = [[0, 4], [0, 1], [1, 2], [3, 2], [3, 1], [4, 5], [5, 6], [6, 4], [0, 7]];
graphLineInit(bg1, lineIndexes);

let bggr1 = document.getElementById("gr1");
let init_posgr1 = [[29, 127], [16, 87], [86, 131], [50, 10], [126, 35]];
let linegr1 = [[0, 1], [0, 2], [0, 3], [0, 4], [1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]];

graphNodeInit(bggr1, init_posgr1);
graphLineInit(bggr1, linegr1);



function step(delta) {
	for (let childIndex = 0; childIndex < lineIndexes.length; childIndex ++){
		let child = children[childIndex];
		let len = lineIndexes.length;
		let circleIndexes = lineIndexes[childIndex];
		child.setAttribute("x1", children[circleIndexes[0]+len].getAttribute("cx"));
		child.setAttribute("y1", children[circleIndexes[0]+len].getAttribute("cy"));
		child.setAttribute("x2", children[circleIndexes[1]+len].getAttribute("cx"));
		child.setAttribute("y2", children[circleIndexes[1]+len].getAttribute("cy"));
	}
	for (let childIndex = lineIndexes.length; childIndex < children.length; childIndex ++){
		let child = children[childIndex];
		let attrx = child.getAttribute("cx");
		let attry = child.getAttribute("cy");
		let distance = dist(init_pos[childIndex - lineIndexes.length], [attrx, attry]);
		child.setAttribute("cx", attrx -  (Math.random() - 0.5) / 2 / (distance == 0? 1: distance));
		child.setAttribute("cy", attry -  (Math.random() - 0.5) / 2 / (distance == 0? 1: distance));
	}
	requestAnimationFrame(evLoop);
}
requestAnimationFrame(evLoop);

let bg2a = document.getElementById("bg2a-nodes");
let bg2aOverlay = document.getElementById("bg2a-overlay");
let bg2b = document.getElementById("bg2b-nodes");
let bg2bOverlay = document.getElementById("bg2b-overlay");
let bg2 = document.getElementById("bg2-nodes");
let bg2Overlay = document.getElementById("bg2-overlay");
let links2 = [[4, 0], [3, 1], [1, 2], [0, 2], [2, 5]];
let init_pos2 = [[29, 127], [75, 87], [86, 131], [0, 100], [-30, 180], [136, 151]];
let init_pos2b = [[50, 100], [86, 131], [-15, 140], [136, 151], [150, 130]];
let links2b = [[2, 0], [0, 1], [1, 3], [3, 4]]
graphNodeInit(bg2a, init_pos2);
graphNodeInit(bg2b, init_pos2b);
graphNodeInit(bg2, init_pos2);

graphLineInit(bg2a, links2);
setTimeout(() => graphLabelInit(bg2a, bg2aOverlay, links2, 0.3), 250);
graphLineInit(bg2b, links2b);
setTimeout(() => graphLabelInit(bg2b, bg2bOverlay, links2b, 0.3, 'w'), 250);
graphLineInit(bg2, links2);
setTimeout(() => graphLabelInit(bg2, bg2Overlay, links2, 0.3), 250);

let init_pos_iso1 = [[30, 30], [30, 60], [30, 90], [30, 120], [60, 30], [60, 60], [60, 90], [60, 120]];
let bg2c = document.getElementById("bg2c-nodes");
let bg2e = document.getElementById("bg2e-nodes");
let bg2eOverlay = document.getElementById("bg2e-overlay");
let links_iso1 = [[0, 4], [1, 5], [2, 6], [3, 7], [0, 5], [0, 6], [1, 4], [1, 7], [2, 4], [2, 7], [3, 6], [3, 5]];
graphNodeInit(bg2c, init_pos_iso1);
graphLineInit(bg2c, links_iso1);
graphNodeInit(bg2e, init_pos_iso1);
graphLineInit(bg2e, links_iso1);
setTimeout(() => graphLabelInit(bg2e, bg2eOverlay, links_iso1, 0.3), 250);

let init_pos_iso2 = [[30, 30], [90, 60], [60, 90], [120, 120], [60, 60], [120, 30], [30, 120], [90, 90]];
let bg2d = document.getElementById("bg2d-nodes");
let bg2dOverlay = document.getElementById("bg2d-overlay");
let links_iso2 = [[0, 4], [1, 5], [2, 6], [3, 7], [0, 5], [0, 6], [1, 4], [1, 7], [2, 4], [2, 7], [3, 6], [3, 5]];
graphNodeInit(bg2d, init_pos_iso2);
graphLineInit(bg2d, links_iso2);


let bg2f = document.getElementById("bg2f-nodes");
let bg2g = document.getElementById("bg2g-nodes");

let bg2fOverlay = document.getElementById("bg2f-overlay");
let bg2gOverlay = document.getElementById("bg2g-overlay");

let init_pos_niso1 = [[30, 30], [30, 60], [60, 30], [60, 60]];
let init_pos_niso2 = [[30, 30], [30, 60], [30, 90], [60, 30], [60, 60], [60, 90]];

let links_niso1 = [[0, 2], [0, 3], [1, 2], [1, 3]];
let links_niso2 = [[0, 3], [0, 4], [0, 5], [1, 3], [1, 4], [1, 5], [2, 3], [2, 4], [2, 5]];

graphNodeInit(bg2f, init_pos_niso1);
graphLineInit(bg2f, links_niso1);

graphNodeInit(bg2g, init_pos_niso2);
graphLineInit(bg2g, links_niso2);



function step2(delta){
	lineTowards(bg2, links2, 0.95);
	requestAnimationFrame(step2);
}

function match2(matches, el, links){
	let children = el.children;
	for (let i = 0; i < matches.length; i++){
		let match = matches[i];
		let m1 = children[match[0] + links.length];
		let m2 = children[match[1] + links.length];
		let [m1cx, m1cy] = [parseInt(m1.getAttribute("cx")), parseInt(m1.getAttribute("cy"))];
		let [m2cx, m2cy] = [parseInt(m2.getAttribute("cx")), parseInt(m2.getAttribute("cy"))];
		let dx = (m1cx - m2cx)/2;
		let dy = (m1cy - m2cy)/2;
		m1.setAttribute("cx", m1cx - dx);
		m1.setAttribute("cy", m1cy - dy);
		m2.setAttribute("cx", m2cx + dx);
		m2.setAttribute("cy", m2cy + dy);
	}
}



function step_iso(delta){
	lineTowards(bg2e, links_iso1, 0.98);
	requestAnimationFrame(step_iso);
}
requestAnimationFrame(step_iso);

Reveal.on('fragmentshown', function(fragment){
	if (fragment.fragment.id ==="e1"){
		for (let childIndex = 0; childIndex < lineIndexes.length; childIndex ++){
			let child = children[childIndex];
			child.style.stroke = "#0000FF";
		}

	} else if (fragment.fragment.id === "v1") {
		for (let childIndex = lineIndexes.length; childIndex < children.length; childIndex ++){
			let child = children[childIndex];
			child.style.stroke = "#FF4444";
		}
	} else if (fragment.fragment.id === "v2") {
		graphLabelInit(bg1, bg1Overlay, lineIndexes, 0.18);
	} else if (fragment.fragment.id === "ev1-2") {
		let matches = [[0, 1], [3, 4]];
		match2(matches, bg2, links2);
		bg2Overlay.style.opacity = "0";
	}  else if (fragment.fragment.id === "ev2-2") {
		addNode(bg2, [150, 130]);
		addLink(bg2, links2, [5, 6]);
		graphLabelInit(bg2, bg2Overlay, links2, 0.3)
	}  else if (fragment.fragment.id === "ev1-3") {
		for (let childIndex = links_iso1.length; childIndex < bg2e.children.length; childIndex ++){
			let child = bg2e.children[childIndex];
			child.setAttribute("cx", init_pos_iso2[childIndex - links_iso1.length][0]);
			child.setAttribute("cy", init_pos_iso2[childIndex - links_iso1.length][1]);
		}
		bg2eOverlay.innerHTML = "";
	}
})

Reveal.on('fragmenthidden', function(fragment){
	if (fragment.fragment.id ==="e1"){
		for (let childIndex = 0; childIndex < lineIndexes.length; childIndex ++){
			let child = children[childIndex];
			child.style.stroke = "#FFFFFF";
		}

	} else if (fragment.fragment.id === "v1") {
		for (let childIndex = lineIndexes.length; childIndex < children.length; childIndex ++){
			let child = children[childIndex];
			child.style.stroke = "#FFFFFF";
		}
	} else if (fragment.fragment.id === "v2") {
		bg1Overlay.innerHTML = "";
	} else if (fragment.fragment.id === "ev1-3") {
		bg2e.innerHTML = "";
		graphNodeInit(bg2e, init_pos_iso1);
		graphLineInit(bg2e, links_iso1);
	}
})



Reveal.on( 'slidetransitionend', event => {
	console.log(event.currentSlide);
	if (event.currentSlide.id  === "sh") {
		evLoop = step2;
	} 
});

