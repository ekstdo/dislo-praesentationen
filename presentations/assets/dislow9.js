Reveal.initialize({
	history: true,
	transition: 'linear',

	mathjax3: {
		mathjax: './assets/mathjax3-es5/tex-svg-full.js',
		tex: {
			inlineMath: [['$', '$'], ['\\(', '\\)']], 
			macros: {
				R: '\\mathbb{R}',
				set: [ '\\left\\{#1 \\; ; \\; #2\\right\\}', 2 ]
			},  
			packages: {'[+]': ['html']}
		},
		svg: {
			fontCache: 'local', 

		}, 
		options: {
		  skipHtmlTags: [ 'script', 'noscript', 'style', 'textarea', 'pre' ]
		},
	},

	// There are three typesetters available
	// RevealMath.MathJax2 (default)
	// RevealMath.MathJax3
	// RevealMath.KaTeX
	//
	// More info at https://revealjs.com/math/
	plugins: [ RevealMath.MathJax3 ]
});

let Aufgabe72 = new Graph("#aufgabe7-2", [[30, 30], [30, 50], [30, 70], [30, 90], [30, 110], [70, 40], [70, 60], [70, 80], [70, 100]], [], {label: true})
Aufgabe72.animate(() => {Aufgabe72.lineTowards(0.95)});

let Aufgabe74 = new Graph("#aufgabe74-problem", [[50, 30], [20, 20], [20, 40]], [[0, 1], [0, 2]], {label: true});

let actions = {
	"aufgabe7-22": function() {
		Aufgabe72.addLink([0, 5]);
		Aufgabe72.addLink([0, 6]);
		Aufgabe72.addLink([0, 7]);
		Aufgabe72.addLink([1, 6]);
		Aufgabe72.addLink([1, 7]);
		Aufgabe72.addLink([1, 8]);
		Aufgabe72.addLink([2, 7]);
		Aufgabe72.addLink([2, 8]);
		Aufgabe72.addLink([2, 5]);
		Aufgabe72.addLink([3, 8]);
		Aufgabe72.addLink([3, 5]);
		Aufgabe72.addLink([3, 6]);
		Aufgabe72.addLink([4, 5]);
		Aufgabe72.addLink([4, 6]);
		Aufgabe72.addLink([4, 7]);
	},
	"aufgabe7-23":function(){
		Aufgabe72.transformEdge([0, 1, 2], edge => edge.style.stroke = "#FF1111");
		undoStack.push("aufgabe7-23");
	},
	"aufgabe7-24":function(){
		Aufgabe72.transformEdge([9, 10, 11], edge => edge.style.stroke = "#FF1111");
		undoStack.push("aufgabe7-24");
	},
	"aufgabe7-25": function(){
		document.querySelector("#aufgabe7-22").style.display = "none";
		document.querySelector("#aufgabe7-23").style.display = "none";
		document.querySelector("#aufgabe7-24").style.display = "none";
	},
	"aufgabe7-27": function(){
		Aufgabe72.transformNode([6, 8], (node, _i) => node.setAttribute("cx", 80));
		Aufgabe72.addLink([7, 8]);
		Aufgabe72.addLink([6, 8]);
		Aufgabe72.transformEdge([15, 16], (edge, _i) => edge.style.stroke = "#7f7")
	},
	"aufgabe7-28": function(){
		Aufgabe72.transformEdge([15, 16], (edge, _i) => edge.style.stroke = "#fff")
		document.querySelector("#aufgabe7-2div").style.transform = "scale(0.7)";
		document.querySelector("#aufgabe7-2div").style.height = "100px";
	},
	"aufgabe7-29": function(){
		let el = document.querySelector("#aufgabe7-2div");
		el.innerHTML = "";
		el.append(MathJax.tex2svg("\\begin{align*}\\text{Sei } G &= (V, E) = (A, B, E) \\text{ bipartit, mit }\\\\ A &= \\{a_i| i \\in [\\frac{n + 1}{2}]\\},\\\\ B &= \\{b_i| i \\in [\\frac{n - 1}{2}]\\} \\text{ und } \\\\E &= \\{a_i b_{(i - 1 \\mod |B|) + 1},\\\\&a_i b_{(i - 1 \\mod |B|) + 2},\\\\ & a_i b_{(i - 1 \\mod |B|) + 3} | i \\in [|A|] \\} \\\\&\\cup \\{b_i b_{i + 1} | i \\in [|B| - 1] \\setminus \\{1, 2\\}\\} \\cup \\{ b_{|B|} b_2 \\}\\end{align*}"));
	}
};
let undo = {
	"aufgabe7-22": function() {
		Aufgabe72.removeLink([0, 5]);
		Aufgabe72.removeLink([0, 6]);
		Aufgabe72.removeLink([0, 7]);
		Aufgabe72.removeLink([1, 6]);
		Aufgabe72.removeLink([1, 7]);
		Aufgabe72.removeLink([1, 8]);
		Aufgabe72.removeLink([2, 7]);
		Aufgabe72.removeLink([2, 8]);
		Aufgabe72.removeLink([2, 5]);
		Aufgabe72.removeLink([3, 8]);
		Aufgabe72.removeLink([3, 5]);
		Aufgabe72.removeLink([3, 6]);
		Aufgabe72.removeLink([4, 5]);
		Aufgabe72.removeLink([4, 6]);
		Aufgabe72.removeLink([4, 7]);
	},
	"aufgabe7-23": function(){Aufgabe72.transformEdge([0, 1, 2], edge => edge.style.stroke = "#FFF");},
	"aufgabe7-24": function(){Aufgabe72.transformEdge([9, 10, 11], edge => edge.style.stroke = "#FFF");},
	"aufgabe7-25": function(){
		document.querySelector("#aufgabe7-22").style.display = "";
		document.querySelector("#aufgabe7-23").style.display = "";
		document.querySelector("#aufgabe7-24").style.display = "";
	},
	"aufgabe7-27": function(){
		Aufgabe72.transformNode([6, 8], (node, _i) => node.setAttribute("cx", 70));
		Aufgabe72.removeLink([7, 8]);
		Aufgabe72.removeLink([6, 8]);
	}
};
let undoStack = [];

Reveal.on('fragmentshown', function(fragment){
	for (let i = 0; i < undoStack.length; i ++){
		undo[undoStack.pop()]();
	}
	if (fragment.fragment.id in actions){
		actions[fragment.fragment.id]();
	}
});

Reveal.on('fragmenthidden', function(fragment){
	if (fragment.fragment.id in undo){
		undo[fragment.fragment.id]();
	}
});
