Reveal.initialize({
	history: true,
	transition: 'linear',

	mathjax3: {
		mathjax: './assets/mathjax3-es5/tex-svg-full.js',
		tex: {
			inlineMath: [['$', '$'], ['\\(', '\\)']], 
			macros: {
				R: '\\mathbb{R}',
				set: [ '\\left\\{#1 \\; ; \\; #2\\right\\}', 2 ]
			},  
			packages: {'[+]': ['html']}
		},
		svg: {
			fontCache: 'local', 

		}, 
		options: {
		  skipHtmlTags: [ 'script', 'noscript', 'style', 'textarea', 'pre' ]
		},
	},

	// There are three typesetters available
	// RevealMath.MathJax2 (default)
	// RevealMath.MathJax3
	// RevealMath.KaTeX
	//
	// More info at https://revealjs.com/math/
	plugins: [ RevealMath.MathJax3 ]
});


let gGraph = new Graph("#gerader-graph", [[30,50],[60, 30], [90,50],  [90, 80],[60, 100], [30, 80]], [[0, 1], [1,2],[2,3],[3, 4],[4, 5], [5,0]], {})
gGraph.animate((graph, delta) => {
	graph.lineTowards(0.95);
})

let pGraph = new Graph("#petersen-graph", [[70, 10], [124, 50], [100, 115], [33, 115], [10, 50], [70, 36], [94, 56], [83, 90], [48, 90], [37, 56]], [[0, 1], [1, 2], [2, 3], [3, 4], [0, 4], [0, 5], [1, 6], [2, 7], [3, 8], [4, 9], [5, 7], [6, 8], [7, 9], [8, 5], [9, 6]], {})
pGraph.animate((graph, delta) => {
	graph.lineTowards(0.95);
})

let gAdj1 = new Graph("#adj1", [[50, 50], [80, 40], [30, 10], [20, 60]], [[0, 3], [1, 2], [2, 3]], {label: true})
gAdj1.animate((graph, delta) => {
	graph.lineTowards(0.95);
})

let knotenInduziert = new Graph("#knoteninduziert", [[50, 50], [30, 65], [10, 20], [45, 80], [67, 78], [24, 85], [80, 45] ], [[0, 6], [1, 6], [4, 6], [2, 1], [2, 4], [5, 3], [3, 4], [3, 2], [3, 1]], {label: true});

let undoStack = [];

let actions = {
	"adj-1m-active": function(){
		gAdj1.transformEdge(0, edge => edge.style.stroke = "#FFFF00");
		document.querySelectorAll(".adj-1m").forEach(el => el.style.fill = "#FFFF00");
	},
	"adj-2m-active": function(){
		gAdj1.transformEdge(1, edge => edge.style.stroke = "#FF1111");
		document.querySelectorAll(".adj-2m").forEach(el => el.style.fill = "#FF1111");
	},
	"adj-3m-active": function(){
		gAdj1.transformEdge(2, edge => edge.style.stroke = "#11FF11");
		document.querySelectorAll(".adj-3m").forEach(el => el.style.fill = "#11FF11");
	},
	"gerader-graph1": function(){
		gGraph.transformNode([0,2,4], node => {
			node.style.stroke = "#55ff55";
		});
		gGraph.transformNode([1,3,5], node => {
			node.style.stroke = "#5555ff";
		});
	},
	"gerader-graph2": function(){
		gGraph.showMatch([[0,2,4]]);
		gGraph.transformNode([0,2,4], node => {
			node.setAttribute('cx', 30);
			node.style.stroke = "#55ff55";
		});
		gGraph.showMatch([[1,3,5]]);
		gGraph.transformNode([1,3,5], node => {
			node.setAttribute('cx', 70);
			node.style.stroke = "#5555ff";
		});
	},
	"petersen-graph1": function(){
		pGraph.transformNode([1, 4, 7, 8], node => {
			node.style.stroke = "#55ff55";
		});
		pGraph.transformNode([0, 2, 9], node => {
			node.style.stroke = "#5555ff";
		});
	},
	"petersen-graph2": function(){
		pGraph.showMatch([[1, 4, 7, 8]]);
		pGraph.transformNode([1, 4, 7, 8], node => {
			node.setAttribute('cx', 120);
			node.setAttribute('cy', 40);
		});
		pGraph.showMatch([[0, 2, 9]]);
		pGraph.transformNode([0, 2, 9], node => {
			node.setAttribute('cx', 85);
			node.setAttribute('cy', 100);
		});
		pGraph.showMatch([[3, 5, 6]]);
		pGraph.transformNode([0, 2, 9], node => {
			node.setAttribute('cx', 40);
			node.setAttribute('cy', 40);
		});
	},

	"knoteninduziert1": function(){
		knotenInduziert.transformNode([3, 5, 6], (node, _index) => { node.style.stroke = "#666"; })
		knotenInduziert.transformEdge((_x) => true, (edge, _index) => { edge.style.stroke = "#666"; })
	},
	"knoteninduziert2": function(){
		knotenInduziert.transformEdge((x) => [0, 1, 2, 4].includes(x[0]) && [0, 1, 2, 4].includes(x[1]), (edge, _index) => { edge.style.stroke = "#FFF"; })
	},
}

let undo = {
	"adj-1m-active": function(){
		gAdj1.transformEdge(0, edge => edge.style.stroke = "#FFFFFF");
		document.querySelectorAll(".adj-1m").forEach(el => el.style.fill = "#FFFFFF");
	},
	"adj-2m-active": function(){
		gAdj1.transformEdge(1, edge => edge.style.stroke = "#FFFFFF");
		document.querySelectorAll(".adj-2m").forEach(el => el.style.fill = "#FFFFFF");
	},
	"adj-3m-active": function(){
		gAdj1.transformEdge(2, edge => edge.style.stroke = "#ffffff");
		document.querySelectorAll(".adj-3m").forEach(el => el.style.fill = "#ffffff");
	},
	"gerader-graph1": function(){
		gGraph.reset();
	},
	"petersen-graph1": function(){
		pGraph.transformNode([1, 4, 7, 8], node => {
			node.style.stroke = "#FFFFFF";
		});
		pGraph.transformNode([0, 2, 9], node => {
			node.style.stroke = "#FFFFFF";
		});
	},
	"petersen-graph2": function(){
		pGraph.reset();
	},
	"knoteninduziert1": function(){
		knotenInduziert.transformNode([3, 5, 6], (node, _index) => { node.style.stroke = "#FFF"; })
		knotenInduziert.transformEdge((_x) => true, (edge, _index) => { edge.style.stroke = "#FFF"; })
	},
	"knoteninduziert2": function(){
		knotenInduziert.transformEdge((x) => 0 in x || 1 in x || 2 in x || 5 in x, (edge, _index) => { edge.style.stroke = "#666"; })
	},
}

Reveal.on('fragmentshown', function(fragment){
	if (fragment.fragment.id in actions){
		for (let i = 0; i < undoStack.length; i ++){
			undo[undoStack.pop()]();
		}
		actions[fragment.fragment.id]();
	}
});

Reveal.on('fragmenthidden', function(fragment){
	if (fragment.fragment.id in undo){
		undo[fragment.fragment.id]();
	}
});
