function lerp(a, b, v){
	return v * a + ( 1- v) * b;
}

function graphNodeInit(parent, positions){
	for (let childIndex = 0; childIndex < positions.length; childIndex ++){
		var newCircle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
		newCircle.setAttribute("cx", positions[childIndex][0]);
		newCircle.setAttribute("cy", positions[childIndex][1]);
		newCircle.setAttribute("r", 8.4538212);
		parent.append(newCircle);
	}
}

function addNode(parent, position){
	var newCircle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
	newCircle.setAttribute("cx", position[0]);
	newCircle.setAttribute("cy", position[1]);
	newCircle.setAttribute("r", 8.4538212);
	parent.append(newCircle);
}

function graphLineInit(parent, indexes){
	let children = parent.children;
	let fst = children[0];
	for (let lineIndex = 0; lineIndex < indexes.length; lineIndex++){
		let circleIndexes = indexes[lineIndex];
		var newLine = document.createElementNS("http://www.w3.org/2000/svg", 'line');
		newLine.setAttribute("x1", children[circleIndexes[0]+lineIndex].getAttribute("cx"));
		newLine.setAttribute("y1", children[circleIndexes[0]+lineIndex].getAttribute("cy"));
		newLine.setAttribute("x2", children[circleIndexes[1]+lineIndex].getAttribute("cx"));
		newLine.setAttribute("y2", children[circleIndexes[1]+lineIndex].getAttribute("cy"));
		newLine.style.stroke = "#FFF";
		parent.insertBefore(newLine, fst);
	}
}

function addLink(parent, indexes, index){
	let children = parent.children;
	let fst = children[indexes.length];
	var newLine = document.createElementNS("http://www.w3.org/2000/svg", 'line');
	newLine.setAttribute("x1", children[index[0]+indexes.length].getAttribute("cx"));
	newLine.setAttribute("y1", children[index[0]+indexes.length].getAttribute("cy"));
	newLine.setAttribute("x2", children[index[1]+indexes.length].getAttribute("cx"));
	newLine.setAttribute("y2", children[index[1]+indexes.length].getAttribute("cy"));
	newLine.style.stroke = "#FFF";
	parent.insertBefore(newLine, fst);
	indexes.push(index);
}

function removeLink(parent, indexes, link){
	let index = indexes.findIndex(el => el[0] == link[0] && el[1] == link[1]);
	parent.removeChild(parent.children[index]);
	indexes.splice(index, 1);
}



function graphLabelInit(parent, overlay, lines, scale, prev = 'v'){
	let children = parent.children;
	for (let childIndex = lines.length; childIndex < children.length; childIndex ++){
		let child = children[childIndex];
		var newText = document.createElementNS("http://www.w3.org/2000/svg", 'foreignObject');
		let attrx = child.getAttribute("cx");
		let attry = child.getAttribute("cy");
		newText.setAttribute("x", attrx);
		newText.setAttribute("y", attry);
		newText.setAttribute("width", 100);
		newText.setAttribute("height", 100);
		newText.style.transform = `scale(${scale})`;
		newText.style.transformOrigin = `${attrx}px ${attry}px`;
		newText.append(MathJax.tex2svg(`${prev}_${childIndex-lines.length +1}`));
		overlay.append(newText);
	}
}

function lineTowards(parent, links_, degree){
	let l = links_.length;
	for (let lineIndex = 0; lineIndex < links_.length; lineIndex++){
		let circleIndexes = links_[lineIndex];
		let el = parent.children[lineIndex];
		el.setAttribute("x1", lerp(el.getAttribute("x1"), parent.children[circleIndexes[0]+l].getAttribute("cx"), degree));
		el.setAttribute("y1", lerp(el.getAttribute("y1"), parent.children[circleIndexes[0]+l].getAttribute("cy"), degree));
		el.setAttribute("x2", lerp(el.getAttribute("x2"), parent.children[circleIndexes[1]+l].getAttribute("cx"), degree));
		el.setAttribute("y2", lerp(el.getAttribute("y2"), parent.children[circleIndexes[1]+l].getAttribute("cy"), degree));
	}
}

function dist(a, b){
	let dx = a[0] - b[0];
	let dy = a[1] - b[1];
	return Math.sqrt(dx * dx + dy * dy);
}

class Graph {
	constructor(parent, nodePositions, indexes, {label, labelSize}) {
		if (typeof(parent) === "string"){
			parent = document.querySelector(parent);
		}
		parent.classList.add("graph-svg");
		this.parent = parent;
		this.initPositions = [...nodePositions];
		this.initIndexes = [...indexes];
		this.indexes = indexes;
		this.overlay = document.createElementNS("http://www.w3.org/2000/svg", 'g');
		this.overlay.classList.add("graph-overlay");
		this.container = document.createElementNS("http://www.w3.org/2000/svg", 'g');
		this.container.classList.add("graph-container");
		this.numNodes = nodePositions.length;
		this.numEdges = indexes.length;
		parent.appendChild(this.overlay);
		parent.appendChild(this.container);
		graphNodeInit(this.container, nodePositions);
		graphLineInit(this.container, indexes);
		this.animationEnabled = false;
		if (label !== undefined) {
			if (label === true) {
				label = 'v';
			}
			if (labelSize === undefined) {
				labelSize = 0.18;
			}
			setTimeout(() => { graphLabelInit(this.container, this.overlay, indexes, labelSize, label) }, 750);
		}
	}

	transformNode(index, transform) {
		if (Array.isArray(index)){
			for (let i = 0; i < index.length; i++)
				transform(this.container.children[index[i] + this.numEdges], i);
		} else 
			transform(this.container.children[index + this.numEdges]);
	}

	transformEdge(index, transform) {
		if (Array.isArray(index)){
			for (let i = 0; i < index.length; i++)
				transform(this.container.children[index[i]], i);
		} else if (typeof(index) === "function") {
			for (let i = 0; i < this.numEdges; i ++){
				if (index(this.indexes[i])){
					transform(this.container.children[i], i);
				}
			}
		} else 
			transform(this.container.children[index]);
	}

	animate(fun){
		this.animationEnabled = true;
		let step = (delta) => {
			if (this.animationEnabled) {
				fun(this, delta);
				requestAnimationFrame(step);
			}
		}
		requestAnimationFrame(step);
	}

	lineTowards(degree){
		lineTowards(this.container, this.indexes, degree);
	}

	nodesRandomMove(d1 = 0.5){
		for (let childIndex = this.numEdges; childIndex < this.container.children.length; childIndex ++){
			let child = this.container.children[childIndex];
			let attrx = child.getAttribute("cx");
			let attry = child.getAttribute("cy");
			let distance = dist(this.initPositions[childIndex - this.numEdges], [attrx, attry]);
			child.setAttribute("cx", attrx - d1 * (Math.random() - 0.5) / (distance == 0? 1: distance));
			child.setAttribute("cy", attry - d1 * (Math.random() - 0.5) / (distance == 0? 1: distance));
		}
	}

	showMatch(matches) {
		let children = this.container.children;
		for (let i = 0; i < matches.length; i++){
			let match = matches[i].map(x => {
				let c = children[x + this.numEdges];
				return [c, parseInt(c.getAttribute("cx")), parseInt(c.getAttribute("cy"))]
			});
			let avgx = match.reduce((acc, el) => acc + el[1], 0) / matches[i].length;
			let avgy = match.reduce((acc, el) => acc + el[2], 0) / matches[i].length;
			match.forEach(([a, _1, _2]) => {
				a.setAttribute("cx", avgx);
				a.setAttribute("cy", avgy);
			})
		}
	}

	reset(){
		this.indexes = this.initIndexes;
		for (let i = 0; i < this.numNodes; i ++){
			let child = this.container.children[i + this.numEdges];
			child.setAttribute("cx", this.initPositions[i][0]);
			child.setAttribute("cy", this.initPositions[i][1]);
			child.style.stroke = "#ffffff";
		}
		this.numEdges = this.indexes.length;
	}

	addLink(link){
		if (this.indexes.findIndex(el => el[0] == link[0] && el[1] == link[1]) != -1) // it already exists
			return;
		this.numEdges += 1;
		addLink(this.container, this.indexes, link);
	}

	removeLink(link){
		this.numEdges -= 1;
		removeLink(this.container, this.indexes, link);
	}
}
